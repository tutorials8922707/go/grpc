package main

import (
	"context"
	"log"

	pb "grpc-tut/my/greet/proto"
)

func doGreet(c pb.GreetServiceClient) {
	log.Println("do Greet was invoked")
	res, err := c.Greet(context.Background(), &pb.GreetRequest{
		FirstName: "Johny",
	})
	if err != nil {
		log.Fatalf("Could not greet: %v\n", err)
	}

	log.Printf("Greeting: %s\n", res.Result)
}
