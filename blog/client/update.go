package main

import (
	"context"
	"log"

	pb "grpc-tut/my/blog/proto"
)

func updateBlog(c pb.BlogServiceClient, id string) {
	log.Println("updateBlog was invoked")

	newBlog := &pb.Blog{
		Id:       id,
		AuthorId: "Not John",
		Title:    "A new title",
		Content:  "Upaded contente",
	}

	_, err := c.UplaodBlog(context.Background(), newBlog)
	if err != nil {
		log.Fatalf("Error happended while updating: %v\n", err)
	}

	log.Println("Blog was updated!")
}
