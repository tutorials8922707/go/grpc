package main

import (
	"context"
	"log"

	pb "grpc-tut/my/calculator/proto"
)

func doSum(c pb.SumServiceClient) {
	sumRequest := pb.SumRequest{
		Lhs: 1,
		Rhs: 2,
	}
	res, err := c.Sum(context.Background(), &sumRequest)
	if err != nil {
		log.Fatalf("Could not sum: %v\n", err)
	}

	log.Printf("Sum of %d and %d is %d", sumRequest.Lhs, sumRequest.Rhs, res.Sum)
}
