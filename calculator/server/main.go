package main

import (
	"log"
	"net"

	"google.golang.org/grpc"

	pb "grpc-tut/my/calculator/proto"
)

var addr string = "127.0.0.1:3005"

type Server struct {
	pb.SumServiceServer
}

func main() {
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("Failed on listen on: %v\n", err)
	}

	s := grpc.NewServer()
	pb.RegisterSumServiceServer(s, &Server{})

	if err = s.Serve(lis); err == nil {
		log.Fatalf("Failed to server: %v", err)
	}
}
