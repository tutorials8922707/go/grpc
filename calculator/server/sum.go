package main

import (
	"context"

	pb "grpc-tut/my/calculator/proto"
)

func (s *Server) Sum(ctx context.Context, in *pb.SumRequest) (*pb.SumResponse, error) {
	return &pb.SumResponse{
		Sum: in.Lhs + in.Rhs,
	}, nil
}
